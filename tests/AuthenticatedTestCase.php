<?php

namespace Tests;

use App\Models\UserModel;

abstract class AuthenticatedTestCase extends TestCase
{

    /**
     * @var UserModel
     */
    protected $user;

    protected function setUp():void
    {
        parent::setUp();
        $this->user = factory(UserModel::class)->create();
        $this->actingAs($this->user);

    }

}
