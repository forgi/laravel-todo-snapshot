<?php

namespace Tests\Feature\AuthController;

use App\Models\UserModel;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class LoginTest extends TestCase
{

    use RefreshDatabase;

    /** @test */
    public function token_returned_when_user_can_log_in()
    {

        /** @var UserModel $user */
        $user = factory(UserModel::class)->create([
            'email'    => 'foo@bar.baz',
            'password' => Hash::make($password = 'secret123'),
        ]);

        $response = $this->postJson(route('auth.login'), [
            'email'    => $user->email,
            'password' => $password,
        ]);
        $response->assertJsonStructure(['access_token', 'token_type', 'expires_in']);
        $token        = $response->json('access_token');

        $this->assertEquals(
            $user->name,
            Auth::setToken($token)->getPayload()->get('name')
        );
    }
}
