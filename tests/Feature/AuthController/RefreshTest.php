<?php

namespace Tests\Feature\AuthController;

use App\Models\UserModel;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class RefreshTest extends TestCase
{

    use RefreshDatabase;

    /** @test */
    public function can_exchange_token_to_a_new_one()
    {
        /** @var UserModel $user */
        $user                   = factory(UserModel::class)->create();
        $tokenToRefresh         = Auth::guard('api')->login($user);
        $oldExpirationTimestamp = Auth::guard('api')->getPayload()->get('exp');

        Carbon::setTestNow(Carbon::now()->addSeconds(10));
        $response = $this->postJson(route('auth.refresh'), [], [
            'Authorization' => "Bearer $tokenToRefresh",
        ]);

        $response->assertJsonStructure(['access_token', 'token_type', 'expires_in']);
        $newToken = $response->json('access_token');

        $this->assertNotEquals($tokenToRefresh, $newToken);
        $newExpirationTimestamp = Auth::guard('api')->setToken($newToken)->getPayload()->get('exp');
        $this->assertEquals(
            $oldExpirationTimestamp + 10,
            $newExpirationTimestamp
        );
    }
}
