<?php

namespace Tests\Feature\TodoAttachmentController;

use App\Models\TodoModel;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Tests\AuthenticatedTestCase;

class StoreTest extends AuthenticatedTestCase
{

    use RefreshDatabase, WithFaker;

    /** @test */
    public function throws_404_when_todo_does_not_exist()
    {

        $response = $this->postJson(route('todo.attachment.store', 1), [
            'attachment' => UploadedFile::fake()->create(
                $filename = Str::random(24) . '.' . $this->faker->fileExtension
            ),
        ]);
        $response->assertNotFound();
    }

    /** @test */
    public function can_store_new_attachments_for_todo()
    {

        Storage::fake('attachments');

        $extension = $this->faker->fileExtension;
        $filename  = Str::random(24) . '.' . $extension;

        $todo     = factory(TodoModel::class)->create();
        $response = $this->postJson(route('todo.attachment.store', $todo), [
            'attachment' => UploadedFile::fake()->create($filename),
        ]);

        $response->assertJsonStructure(['url']);
        $this->assertTrue(
            Str::endsWith($response->json('url'), '1.' . $extension)
        );

        Storage::disk('attachments')->assertExists('1.' . $extension);

        $this->assertDatabaseHas('todo_attachments', [
            'todo_id'         => $todo->id,
            'client_filename' => $filename,
            'extension'       => $extension,
        ]);
    }

}
