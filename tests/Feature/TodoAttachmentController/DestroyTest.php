<?php

namespace Tests\Feature\TodoAttachmentController;

use App\Models\TodoAttachmentModel;
use App\Models\TodoModel;
use Illuminate\Contracts\Filesystem\Filesystem;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Support\Facades\Storage;
use Mockery;
use PHPUnit\Framework\MockObject\MockObject;
use RuntimeException;
use Tests\AuthenticatedTestCase;
use Tests\TestCase;

class DestroyTest extends AuthenticatedTestCase
{

    use RefreshDatabase;

    /** @test */
    public function when_attachment_not_found_nothing_will_happen()
    {

        $todo = factory(TodoModel::class)->create();

        $response = $this->deleteJson(route('todo.attachment.destroy', [
            $todo,
            1,
        ]));
        $response->assertOk();
    }

    /** @test */
    public function attachment_is_deleted_when_exists()
    {
        $extension = 'bar';
        $filename  = 1 . '.' . $extension;

        /** @var TodoAttachmentModel $attachment */
        $attachment = factory(TodoAttachmentModel::class)->create(['extension' => $extension]);

        Storage::fake('attachments');
        Storage::disk('attachments')->put($filename, 'file content');

        $response = $this->deleteJson(route('todo.attachment.destroy', [
            $attachment->todo_id,
            $attachment->id,
        ]));
        $response->assertOk();

        Storage::disk('attachments')->assertMissing($filename);
        $this->assertDatabaseMissing('todo_attachments', [
            'id' => $attachment->id,
        ]);
    }

    /** @test */
    public function when_file_deletion_crashes_attachment_will_not_get_deleted()
    {

        $this->expectException(RuntimeException::class);

        $extension = 'bar';

        /** @var TodoAttachmentModel $attachment */
        $attachment = factory(TodoAttachmentModel::class)->create(['extension' => $extension]);

        Storage::fake('attachments');
        Storage::shouldReceive('disk')
            ->andReturn(Mockery::mock(Filesystem::class, function (Mockery\MockInterface $mock) use ($attachment) {

                $mock->shouldReceive('exists')
                    ->withArgs([$attachment->id . '.' . $attachment->extension])
                    ->once()
                    ->andReturn(true);

                $mock->shouldReceive('delete')
                    ->withArgs([$attachment->id . '.' . $attachment->extension])
                    ->once()
                    ->andReturn(false);
            }));

        $response = $this->deleteJson(route('todo.attachment.destroy', [
            $attachment->todo_id,
            $attachment->id,
        ]));

        $this->assertDatabaseHas('todo_attachments', ['id' => $attachment->id]);

        throw $response->exception;
    }
}
