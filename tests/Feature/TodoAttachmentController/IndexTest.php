<?php

namespace Tests\Feature\TodoAttachmentController;

use App\Models\TodoAttachmentModel;
use App\Models\TodoModel;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\AuthenticatedTestCase;

class IndexTest extends AuthenticatedTestCase
{

    use RefreshDatabase;

    /** @test */
    public function throws_404_when_todo_does_not_exists()
    {
        $response = $this->getJson(route('todo.attachment.index', 1));
        $response->assertNotFound();
    }

    /** @test */
    public function can_list_attachments_of_a_todo()
    {
        $todo = factory(TodoModel::class)->create(['owner_id' => $this->user->id]);
        factory(TodoAttachmentModel::class)
            ->times(10)
            ->create(['todo_id' => $todo->id]);

        $response = $this->getJson(route('todo.attachment.index', $todo));
        $response->assertOk();
        $response->assertJsonCount(10);
        $response->assertJsonStructure([
            '*' => [
                'id',
                'uploaded_at',
                'client_filename',
                'extension',
                'url',
            ],
        ]);
    }

}
