<?php

namespace Tests\Feature\TodoController;

use App\Models\TodoModel;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\AuthenticatedTestCase;
use Tests\TestCase;

class DestroyTest extends AuthenticatedTestCase
{

    use RefreshDatabase;

    /** @test */
    public function only_owner_can_destroy_a_todo()
    {

        /* @var TodoModel $todo */
        $todo = factory(TodoModel::class)->create();

        $response = $this->deleteJson(route('todo.destroy',$todo));
        $response->assertStatus(403);
    }

    /** @test */
    public function can_destroy_todo_when_it_exists()
    {

        /* @var TodoModel $todo */
        $todo = factory(TodoModel::class)->create(['owner_id' => $this->user->id]);

        $response = $this->deleteJson(route('todo.destroy',$todo));
        $response->assertStatus(200);

        $this->assertDatabaseMissing('todos',[
            'id' => $todo->id,
        ]);
    }

}
