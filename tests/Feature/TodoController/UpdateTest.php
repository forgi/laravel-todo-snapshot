<?php

namespace Tests\Feature\TodoController;

use App\Models\TodoModel;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Symfony\Component\HttpFoundation\Response;
use Tests\AuthenticatedTestCase;
use Tests\TestCase;

class UpdateTest extends AuthenticatedTestCase
{

    use RefreshDatabase,WithFaker;

    /** @test */
    public function when_todo_does_not_exist_then_fail_with_404()
    {

        $response = $this->putJson(route('todo.update',1),[
            'name'        => $this->faker->realText(50),
            'description' => $this->faker->realText(50),
            'status'      => 'in_progress',
        ]);

        $response->assertStatus(404);
    }

    /** @test */
    public function can_update_when_exists()
    {

        $todo = factory(TodoModel::class)->create(['owner_id' => $this->user->id]);

        $response = $this->putJson(route('todo.update',$todo),[
            'name'        => $newName = $this->faker->realText(50),
            'description' => $newDescription = $this->faker->realText(50),
            'status'      => $newStatus = 'in_progress',
        ]);

        $response->assertStatus(200);
        $this->assertDatabaseHas('todos',[
            'id'          => $todo->id,
            'name'        => $newName,
            'description' => $newDescription,
            'status'      => $newStatus,
        ]);
    }

    /** @test */
    public function cross_owner_todo_update_is_not_possible()
    {

        $todo = factory(TodoModel::class)->create();

        $response = $this->putJson(route('todo.update',$todo),[
            'name'        => $newName = $this->faker->realText(50),
            'description' => $newDescription = $this->faker->realText(50),
            'status'      => $newStatus = 'in_progress',
        ]);

        $response->assertStatus(403);
    }

    /** @test */
    public function status_cannot_be_new_when_it_was_in_progress()
    {

        $todo = factory(TodoModel::class)
            ->state('status.in_progress')
            ->create(['owner_id' => $this->user->id]);

        $response = $this->putJson(route('todo.update',$todo),[
            'status' => $newStatus = 'new',
        ]);

        $response->assertStatus(409);

        $response->assertJsonStructure(['message','todoId']);
        $response->assertJson(
            [
                'message' => 'Status of the given todo cannot demoted!',
                'todoId'  => $todo->id,
            ]
        );

        $this->assertDatabaseHas('todos',[
            'id'     => $todo->id,
            'status' => 'in_progress',
        ]);

    }

}
