<?php

namespace Tests\Feature\TodoController;

use App\Models\TodoModel;
use App\Models\UserModel;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Symfony\Component\HttpFoundation\Response;
use Tests\AuthenticatedTestCase;
use Tests\TestCase;

class StoreTest extends AuthenticatedTestCase
{

    use RefreshDatabase,WithFaker;

    /** @test */
    public function can_store_todo()
    {

        $name        = $this->faker->realText(50);
        $description = $this->faker->paragraph(3);
        $status      = $this->faker->randomElement(['new','in_progress','done']);

        $response = $this->postJson(route('todo.store'),[
            'name'        => $name,
            'description' => $description,
            'status'      => $status,
        ]);

        $response->assertStatus(201);
        $response->assertJsonStructure(['id']);
        $generatedId = $response->json('id');

        $this->assertNotNull($generatedId);
        $this->assertIsNumeric($generatedId);

        $this->assertDatabaseHas('todos',[
            'id'          => $generatedId,
            'name'        => $name,
            'description' => $description,
            'status'      => $status,
            'owner_id'    => $this->user->id,
        ]);
    }

    /** @test */
    public function cannot_store_todo_when_name_is_not_given()
    {

        $description = $this->faker->paragraph(3);
        $status      = $this->faker->randomElement(['new','in_progress','done']);

        $response = $this->postJson(route('todo.store'),[
            'description' => $description,
            'status'      => $status,
        ]);

        $response->assertStatus(422);
        $response->assertJsonValidationErrors('name');

        $this->assertEquals(0,TodoModel::query()->count());
    }

}
