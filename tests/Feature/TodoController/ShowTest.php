<?php

namespace Tests\Feature\TodoController;

use App\Models\TodoModel;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\AuthenticatedTestCase;
use Tests\TestCase;

class ShowTest extends AuthenticatedTestCase
{

    use RefreshDatabase;

    /** @test */
    public function cross_owner_show_is_not_possible()
    {

        /* @var TodoModel $todo */
        $todo = factory(TodoModel::class)->create();

        $response = $this->getJson(route('todo.show',$todo));

        $response->assertStatus(403);
    }

    /** @test */
    public function todo_shown_with_same_name_and_description_and_status()
    {

        /* @var TodoModel $todo */
        $todo = factory(TodoModel::class)->create(['owner_id' => $this->user->id]);

        $response = $this->getJson(route('todo.show',$todo));

        $response->assertStatus(200);
        $response->assertJson(
            [
                'name'        => $todo->name,
                'description' => $todo->description,
                'status'      => $todo->status,
            ]
        );
    }

}
