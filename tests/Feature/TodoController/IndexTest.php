<?php

namespace Tests\Feature\TodoController;

use App\Models\TodoModel;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\AuthenticatedTestCase;
use Tests\TestCase;

class IndexTest extends AuthenticatedTestCase
{

    use RefreshDatabase;

    /** @test */
    public function cross_owner_listing_is_not_possible()
    {

        factory(TodoModel::class)->create();
        $response = $this->getJson(route('todo.index'));
        $response->assertJsonCount(0,'data');
    }

    /** @test */
    public function when_status_given_then_can_filter_on_it()
    {

        factory(TodoModel::class)->times(10)->state('status.new')->create(['owner_id' => $this->user->id]);
        factory(TodoModel::class)->times(11)->state('status.in_progress')->create(['owner_id' => $this->user->id]);
        factory(TodoModel::class)->times(12)->state('status.done')->create(['owner_id' => $this->user->id]);

        $response = $this->getJson(route('todo.index',['status' => 'new']));
        $response->assertJsonCount(10,'data');
        $response->assertJsonFragment(['status' => 'new']);

        $response = $this->getJson(route('todo.index',['status' => 'in_progress']));
        $response->assertJsonCount(11,'data');
        $response->assertJsonFragment(['status' => 'in_progress']);

        $response = $this->getJson(route('todo.index',['status' => 'done']));
        $response->assertJsonCount(12,'data');
        $response->assertJsonFragment(['status' => 'done']);
    }

    /** @test */
    public function lists_todos_with_id_name_and_status()
    {

        $perPage = 15;

        /* @var \Illuminate\Database\Eloquent\Collection $todos */
        $todos = factory(TodoModel::class)
            ->times(50)
            ->create(['owner_id' => $this->user->id]);

        $response = $this->getJson(route('todo.index',['count' => $perPage]));

        $response->assertJsonStructure(
            [
                'data' => ['*' => ['id','name','status']],
            ]
        );

        $response->assertJsonCount($perPage,'data');

        $payload = $response->json();
        foreach($payload as $data){
            $this->assertArrayNotHasKey('description',$data);
        }
    }

}
