<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use InvalidArgumentException;

/**
 * Class TodoModel
 *
 * @package App\Models
 *
 * @property int                                           $id
 * @property \Illuminate\Support\Carbon                    $created_at
 * @property \Illuminate\Support\Carbon                    $updated_at
 * @property string                                        $name
 * @property string                                        $description
 * @property string                                        $status
 *
 * @property-read \App\Models\UserModel                    $owner
 * @property-read \Illuminate\Database\Eloquent\Collection $attachments
 * @property int                                           $owner_id
 *
 * @see \CreateTodosTable
 */
class TodoModel extends Model
{

    const STATUSES = [
        'new',
        'in_progress',
        'done',
    ];

    protected $table = 'todos';

    protected $fillable = [
        'name',
        'description',
        'status',
    ];

    /**
     * @param string $status
     */
    protected function setStatusAttribute(string $status)
    {

        if (!in_array($status, self::STATUSES)) {
            $statuses = implode(',', self::STATUSES);
            throw new InvalidArgumentException("Given status [$status] is invalid! Must be one of the followings: $statuses");
        }

        $this->attributes['status'] = $status;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function owner()
    {

        return $this->belongsTo(UserModel::class, 'owner_id', 'id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function attachments()
    {
        return $this->hasMany(TodoAttachmentModel::class, 'todo_id');
    }

}
