<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class TodoAttachmentModel
 * @package App\Models
 *
 * @property-read int                   $id
 * @property \Illuminate\Support\Carbon $created_at
 * @property \Illuminate\Support\Carbon $updated_at
 * @property int                        $todo_id
 * @property string                     $client_filename
 * @property string                     $extension
 *
 * @property-read \App\Models\TodoModel $todo
 */
class TodoAttachmentModel extends Model
{

    protected $table = 'todo_attachments';

    protected $fillable = [
        'client_filename',
        'extension',
        'todo_id'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function todo()
    {
        return $this->belongsTo(TodoModel::class, 'todo_id');
    }

}
