<?php

namespace App\Http\Controllers;

use App\Exceptions\InvalidTodoStatusDemotion;
use App\Http\Requests\TodoStoreRequest;
use App\Http\Resources\Todos;
use App\Models\TodoModel;
use Illuminate\Database\DatabaseManager;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Str;
use Illuminate\Validation\Rule;

class TodoController extends Controller
{

    /**
     * @param \Illuminate\Http\Request $request
     *
     * @return \App\Http\Resources\Todos
     * @throws \Illuminate\Validation\ValidationException
     */
    public function index(Request $request)
    {

        $this->validate($request,[
            'count'  => ['sometimes','numeric','min:1','max:100'],
            'status' => ['sometimes','nullable',Rule::in(TodoModel::STATUSES)],
        ]);

        $select = ['id','name','status', 'owner_id'];
        $todos  = TodoModel::query()
                           ->select($select)
                           ->where('owner_id',Auth::user()->id);

        // Lookup todos based on the given status.
        if($status = $request->get('status')){

            $todos->where('status',$status);
        }

        return new Todos(
            $todos->paginate($request->get('count'))
        );
    }

    /**
     * @param \App\Http\Requests\TodoStoreRequest $request
     *
     * @return \Illuminate\Http\JsonResponse
     *
     */
    public function store(TodoStoreRequest $request)
    {

        /* @var \App\Models\UserModel $user */
        $user = Auth::user();
        $todo = $user->todos()->save(new TodoModel($request->validated()));

        return response()->json(['id' => $todo->id],201);
    }

    /**
     * @param int $todoId
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function show(int $todoId)
    {

        /* @var TodoModel $todo */
        $todo = TodoModel::query()->findOrFail($todoId);

        Gate::authorize('see',$todo);

        return response()
            ->json(
                [
                    'name'        => $todo->name,
                    'description' => $todo->description,
                    'status'      => $todo->status,
                ]
            );
    }

    /**
     * @param int $todoId
     *
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function destroy(int $todoId)
    {

        /* @var TodoModel $todo */
        $todo = TodoModel::query()->findOrFail($todoId,['id','owner_id']);

        Gate::authorize('destroy',$todo);

        $todo->delete();

        return response()
            ->json(['message' => 'Ok'],200);
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param int                      $todoId
     *
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function update(Request $request,int $todoId)
    {

        /* @var TodoModel $todo */
        $todo = TodoModel::query()->findOrFail($todoId);

        Gate::authorize('edit',$todo);

        $this->validate($request,[
            'name'        => ['nullable','string','max:50','min:5'],
            'description' => ['nullable','string','max:150000'],
            'status'      => ['nullable','string',Rule::in(TodoModel::STATUSES)],
        ]);

        if($name = $request->input('name')){
            $todo->name = $name;
        }

        if($description = $request->input('description')){
            $todo->description = $description;
        }

        if($status = $request->input('status')){

            if($todo->status === 'in_progress' && $status === 'new'){
                throw new InvalidTodoStatusDemotion($todo);
            }

            $todo->status = $status;
        }

        $todo->save();

        return response()->json(['message' => 'Todo updated!'],200);
    }

}
