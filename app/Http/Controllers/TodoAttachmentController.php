<?php

namespace App\Http\Controllers;

use App\Models\TodoAttachmentModel;
use App\Models\TodoModel;
use Illuminate\Contracts\Filesystem\Filesystem;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Storage;
use RuntimeException;

class TodoAttachmentController extends Controller
{

    /**
     * @var \Illuminate\Contracts\Filesystem\Filesystem
     */
    protected $disk;

    /**
     * TodoAttachmentController constructor.
     *
     * @param \Illuminate\Contracts\Filesystem\Filesystem $disk
     */
    public function __construct(Filesystem $disk)
    {
        $this->disk = $disk;
    }

    /**
     * @param $todoId
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index($todoId)
    {

        /** @var TodoModel $todo */
        $todo = TodoModel::query()
            ->findOrFail($todoId, ['id', 'owner_id']);

        Gate::authorize('see', $todo);

        return response()->json(
            $todo
                ->attachments
                ->map(function (TodoAttachmentModel $attachment) {

                    return [
                        'id'              => $attachment->id,
                        'uploaded_at'     => $attachment->created_at,
                        'client_filename' => $attachment->client_filename,
                        'extension'       => $attachment->extension,
                        'url'             => $this->disk->url($attachment->id . '.' . $attachment->extension),
                    ];
                })
        );
    }

    /**
     * @param                          $todoId
     * @param \Illuminate\Http\Request $request
     *
     * @return mixed
     * @throws \Illuminate\Validation\ValidationException
     */
    public function store($todoId, Request $request)
    {

        $this->validate($request, [
            'attachment' => ['required', 'file'],
        ]);

        /** @var TodoModel $todo */
        $todo = TodoModel::query()->findOrFail($todoId, ['id']);

        $attachment = $request->file('attachment');

        return DB::transaction(function () use ($todo, $attachment) {

            /** @var TodoAttachmentModel $todoAttachment */
            $todoAttachment = $todo->attachments()->save(
                new TodoAttachmentModel([
                    'client_filename' => $attachment->getClientOriginalName(),
                    'extension'       => $extension = $attachment->getClientOriginalExtension(),
                ])
            );

            $filename = $todoAttachment->id . '.' . $extension;

            $this->disk->putFileAs('', $attachment, $filename);
            return response()->json(['url' => $this->disk->url($filename)]);
        });


    }

    public function destroy($todoId, $attachmentId)
    {

        /** @var TodoAttachmentModel $attachment */
        if ($attachment = TodoAttachmentModel::query()->find($attachmentId)) {

            DB::transaction(function () use ($attachment) {

                $attachment->delete();
                $filename = $attachment->id . '.' . $attachment->extension;

                if ($this->disk->exists($filename)) {

                    if (!$this->disk->delete($filename)) {

                        throw new RuntimeException("Could not delete attachment file [$filename]");
                    }
                }
            });
        }

        return response()->json(['message' => 'ok'], 200);
    }

}
