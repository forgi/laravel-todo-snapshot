<?php

namespace App\Http\Requests;

use App\Models\TodoModel;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class TodoStoreRequest extends FormRequest
{
    
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        
        return true;
    }
    
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        
        return [
            'name'        => ['required','string','max:50','min:5'],
            'description' => 'required|string|max:150000',
            'status'      => ['required','string',Rule::in(TodoModel::STATUSES)],
        ];
    }
    
}
