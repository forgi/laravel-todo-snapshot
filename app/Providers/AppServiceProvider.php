<?php

namespace App\Providers;

use App\Http\Controllers\TodoAttachmentController;
use Illuminate\Contracts\Filesystem\Filesystem;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {

        $this->app
            ->when(TodoAttachmentController::class)
            ->needs(Filesystem::class)
            ->give(function () {
                return Storage::disk('attachments');
            });
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
    }
}
