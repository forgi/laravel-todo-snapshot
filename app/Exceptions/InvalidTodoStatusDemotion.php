<?php

namespace App\Exceptions;

use App\Models\TodoModel;
use Illuminate\Contracts\Support\Responsable;
use RuntimeException;
use Throwable;

class InvalidTodoStatusDemotion extends RuntimeException implements Responsable
{

    /**
     * @var \App\Models\TodoModel
     */
    public $todo;

    /**
     * InvalidTodoStatusDemotion constructor.
     *
     * @param \App\Models\TodoModel $todo
     */
    public function __construct(TodoModel $todo)
    {

        parent::__construct('Status of the given todo cannot demoted!');
        $this->todo = $todo;
    }

    /**
     * @inheritDoc
     */
    public function toResponse($request)
    {

        return response()->json(
            [
                'message' => $this->getMessage(),
                'todoId'  => $this->todo->id,
            ],
            409
        );
    }

    /**
     * @return \App\Models\TodoModel
     */
    public function getTodo():TodoModel
    {

        return $this->todo;
    }

}
