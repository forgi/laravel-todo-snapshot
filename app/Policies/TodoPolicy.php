<?php

namespace App\Policies;

use App\Models\TodoModel;
use App\Models\UserModel;
use Illuminate\Auth\Access\HandlesAuthorization;

class TodoPolicy
{

    use HandlesAuthorization;

    /**
     * @param \App\Models\UserModel $user
     * @param \App\Models\TodoModel $todo
     *
     * @return bool
     */
    public function edit(UserModel $user,TodoModel $todo)
    {

        return (int)$user->id === (int)$todo->owner_id;
    }

    /**
     * @param \App\Models\UserModel $user
     * @param \App\Models\TodoModel $todo
     *
     * @return bool
     */
    public function see(UserModel $user,TodoModel $todo)
    {

        return (int)$user->id === (int)$todo->owner_id;
    }

    /**
     * @param \App\Models\UserModel $user
     * @param \App\Models\TodoModel $todo
     *
     * @return bool
     */
    public function destroy(UserModel $user,TodoModel $todo)
    {

        return (int)$user->id === (int)$todo->owner_id;

    }

}
