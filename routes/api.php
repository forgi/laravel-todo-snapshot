<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::prefix('auth')
    ->group(function () {

        Route::post('login', 'AuthController@login')->name('auth.login');
        Route::post('logout', 'AuthController@logout')->name('auth.logout');
        Route::post('refresh', 'AuthController@refresh')->name('auth.refresh');
        Route::post('me', 'AuthController@me')->name('auth.me');

    });

Route::middleware('auth')->group(function () {

    Route::resource('todo', 'TodoController')
        ->names('todo');

    Route::resource('todo/{todoId}/attachment', 'TodoAttachmentController')
        ->only('index', 'store', 'destroy')
        ->names('todo.attachment');


});
