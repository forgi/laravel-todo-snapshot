<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

/**
 * Class CreateTodosTable
 *
 * @see \App\Models\TodoModel
 */
class CreateTodosTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::create('todos',function(Blueprint $table){

            $table->bigIncrements('id');
            $table->timestamps();
            $table->string('name',50)->index();
            $table->mediumText('description');
            $table->enum('status',['new','in_progress','done']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

        Schema::dropIfExists('todos');
    }

}
