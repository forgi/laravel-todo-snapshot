<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\UserModel;
use Faker\Generator as Faker;

$factory->define(UserModel::class,function(Faker $faker){

    return [
        'email'    => $faker->email,
        'name'     => $faker->name,
        'password' => \Illuminate\Support\Facades\Hash::make('password'),
    ];
});
