<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\TodoModel;
use Faker\Generator as Faker;

$factory->define(TodoModel::class,function(Faker $faker){

    return [
        'name'        => $faker->realText(50),
        'description' => $faker->paragraphs(3,true),
        'status'      => $faker->randomElement(TodoModel::STATUSES),
        'owner_id'    => factory(\App\Models\UserModel::class)->lazy(),
    ];
});

$factory->state(TodoModel::class,'status.new',function(){

    return ['status' => 'new'];
});

$factory->state(TodoModel::class,'status.in_progress',function(){

    return ['status' => 'in_progress'];
});

$factory->state(TodoModel::class,'status.done',function(){

    return ['status' => 'done'];
});
