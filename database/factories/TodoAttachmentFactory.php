<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\TodoAttachmentModel;
use App\Models\TodoModel;
use Faker\Generator as Faker;
use Illuminate\Support\Str;

$factory->define(TodoAttachmentModel::class, function (Faker $faker) {
    return [
        'client_filename' => Str::random(24),
        'extension'       => $faker->fileExtension,
        'todo_id'         => factory(TodoModel::class)->lazy(),
    ];
});
